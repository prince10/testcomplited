//
//  DestinationViewController.h
//  googleTest1
//
//  Created by Prince on 04/11/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DestinationViewController : UIViewController
@property (strong,nonatomic) NSString *deststr;

@property (strong,nonatomic) NSString *mystate;

@property (strong,nonatomic) NSString *countryMy;

@property (strong,nonatomic) NSString *address;
@end
